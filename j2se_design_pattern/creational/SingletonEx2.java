package com.test.j2se_design_pattern.creational;

//Early instantiation using implementation with static field.


class SingletonEx2
{
	private static SingletonEx2 instance = new SingletonEx2();

	private SingletonEx2()
	{
		System.out.println("Singleton(): Initializing Instance");
	}

	public static SingletonEx2 getInstance()
	{    
		return instance;
	}

	public void doSomething()
	{
		System.out.println("doSomething(): Singleton does something!");
	}
}


