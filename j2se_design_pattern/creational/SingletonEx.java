package com.test.j2se_design_pattern.creational;


//Lazy instantiation using double locking mechanism.  Uses in multi threading environment


public class SingletonEx
{
	private static SingletonEx instance;

	private SingletonEx()

	{
		System.out.println("SingletonEx(): Initializing Instance");
	}

	public static SingletonEx getInstance()
	{
		if (instance == null)
		{
			synchronized(SingletonEx.class)
			{
				if (instance == null)
				{
					System.out.println("getInstance(): First time getInstance was invoked!");
					
					instance = new SingletonEx();
				}
			}            
		}

		return instance;
	}

	public void doSomething()
	{
		System.out.println("doSomething(): SingletonEx does something!");
	}
}

