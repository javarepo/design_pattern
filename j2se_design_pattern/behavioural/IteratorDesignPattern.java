package com.test.j2se_design_pattern.behavioural;

import java.util.Iterator;

public class IteratorDesignPattern {
	
	String name[] = { "one" , "two" , "three" , "four" } ; 
	
	private class IteratorImpl implements Iterator<Object> {
		
		int i = 0; 

		@Override
		public boolean hasNext() {
			
			if ( i < name.length ) 
				
				return true; 
			
			else return false; 
			
		}

		@Override
		public Object next() {
			
			if ( this.hasNext() ) {
				
				return name[i++]; 
			}
			
			return null;
		}
		
	}
	
	public static void main(String[] args) {
		
		IteratorDesignPattern iterDP = new IteratorDesignPattern(); 
		
		IteratorImpl iteratorImpl = iterDP.new IteratorImpl(); 
		
		while ( iteratorImpl.hasNext() )
		
		System.out.println(iteratorImpl.next());  

	}

}
